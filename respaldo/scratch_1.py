
import os

def menu():
    """
    Función que limpia la pantalla y muestra nuevamente el menu
    """
    os.system('clear')  # NOTA para windows tienes que cambiar clear por cls
    print ("Selecciona una opción")
    print ("\t1 - Trasferir datos GPS desde el celular hacia la Raspberry")
    print ("\t2 - Enviar archivo")
    print ("\t3 - Salir")


######################################Leer UART#############################
def transmitdata():
    ser = serial.Serial ("/dev/ttyAMA0", 9600)    #Open port with baud rate
    with open('DatosGPS.csv', newline='') as File:  
        reader = csv.reader(File)
        for row in reader:
            for a in row:
                ser.write(str.encode(a))
                sleep(0.09)
                print(a)
        ser.write(str.encode('zyxp'))

def exportdata():
    try:
        s = pxssh.pxssh()
        hostname = '172.24.72.31'
        username = 'root'
        password = 'admin'
        s.login(hostname, username, password, port=2222)
        s.sendline(scpcommand)
        s.prompt()
        s.sendline('pi')
        s.prompt()
        print(s.before)
        s.logout()
    except pxssh.ExceptionPxssh as e:
        print("pxssh failed on login.")
        print(e)
        
while True:
    # Mostramos el menu
    menu()
    # solicituamos una opción al usuario
    opcionMenu = input("Escoja la opcion que desea ejecutar:")

    if opcionMenu == "1":
        #LLAMAR LA FUNCION DE TRANSFERIR ARCHIVO
        print ("")
        input("Has pulsado la opción 1...\npulsa una tecla para continuar")
    elif opcionMenu == "2":
        #LLAMAR LA FUNCION ENVIAR ARCHIVO
        print ("")
        input("Has pulsado la opción 2...\npulsa una tecla para continuar")
    elif opcionMenu == "3":
        break
    else:
        print ("")
        input("No has pulsado ninguna opción correcta...\nPulsa una tecla para continuar")
        