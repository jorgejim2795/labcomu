from pexpect import pxssh
import subprocess 
import getpass
import time
import serial
from time import sleep
import csv
import os

def menu():
    """
    Función que limpia la pantalla y muestra nuevamente el menu
    """
    subprocess.call('clear',shell=True)
    print ("Selecciona una opción")
    print ("\t1 - Trasferir datos GPS desde el celular hacia la Raspberry")
    print ("\t2 - Enviar archivo")
    print ("\t3 - Actualizar repositorio de Gitlab")
    print ("\t4 - Graficar Overhead vs distancia")
    print ("\t5 - Salir")
    
def exportdata():
    IPadd = subprocess.getoutput('hostname -I')

    scpcommand0 ='scp '
    command1    =' pi@'
    scpcommand2=':/home/pi/labcomu'
    try:
        s = pxssh.pxssh()
        hostname = input("Ingrese el numero de IP de la conexion remota:")
        #hostname = '172.24.106.222'
        archivo = input("Ingrese el nombre del Archivo remoto:")
        scpcommand=scpcommand0+ archivo+command1+IPadd[0:14]+scpcommand2
        username = 'root'
        password = 'admin'
        s.login(hostname, username, password, port=2222)
        s.sendline('cd Android/data/com.mendhak.gpslogger/files')
        print(scpcommand)
        s.sendline(scpcommand)
        s.prompt()
        s.sendline('pi')
        s.prompt()
        print(s.before)
        s.logout()
    except pxssh.ExceptionPxssh as e:
        print("pxssh failed on login.")
        print(e)
    
    gh=input('\n\nPresione una tecla para continuar')    
        
def gitlab ():
    date= subprocess.getoutput('date +%Y-%m-%d-%H-%M')
    subprocess.call('git add .',shell=True)
    subprocess.call('git commit -m "'+ date+'"',shell=True)
    subprocess.call('git push',shell=True)

    gh=input('\n\nPresione una tecla para continuar')
    

def createFile():
    info=subprocess.call('find DatosGPS.csv',shell=True)
    if (info==0):
        subprocess.call('rm DatosGPS.csv', shell=True)
        subprocess.call('touch DatosGPS.csv', shell=True)
    else:
        subprocess.call('touch DatosGPS.csv', shell=True)       
    with open('20190210.csv', newline='') as File:  
        reader = csv.reader(File)
        for row in reader:
            for counter in row[1:3]:
                File2=open('DatosGPS.csv','a')
                File2.write(str(counter)+',')
            File2.write('\n')

        File2.close()
#######################################################################3
def createFileoverhead():
    
    hora= subprocess.getoutput('date +%H-%M')
    print(hora)
    subprocess.call('touch ~/labcomu/over/OverheadDATA'+hora+'.csv', shell=True)
    return hora
    
def WriteOverdata(dat,ff,distancia):
    File3=open('over/OverheadDATA'+str(distancia)+ff+'.csv','a')
    File3.write(dat+'\n')
    File3.close()
    
######################################Leer UART#############################
def transmitdata(ff,distancia):
    ser = serial.Serial ("/dev/ttyAMA0", 9600,timeout=1)    #Open port with baud rate
    with open('DatosGPS.csv', newline='') as File:  
        reader = csv.reader(File)
        for row in reader:
            for a in row:
                if (a==''):
                    break
                else:
                    while 1:
                        ser.write(str.encode(a+','))
                        WriteOverdata(a,ff,distancia)
                        sleep(0.1)
                        print('Dato enviado= '+a+',')
                        received_data = ser.read(1)
                        sleep(0.05)
                        if (received_data.decode()!=''):
                            break
                    data_left = ser.inWaiting()             #check for remaining byte
                    received_data += ser.read(data_left)
                    print ('Dato recibido= '+received_data.decode())
                    if (received_data.decode()==(a+',')):
                        ser.write(str.encode('si'))
                        WriteOverdata('si',ff,distancia)
                        sleep(0.2)
                    elif  (received_data.decode()!=(a+',')):
                        ser.write(str.encode(a+','))
                        WriteOverdata(a,ff,distancia)
                        sleep(0.3)
                        print('.....Reenviando dato......')
                    
        ser.write(str.encode('zyxp'))
        print('El archivo ha sido enviado con exito')

def goverhead(distancia,overhead):
    subprocess.call('touch ~/labcomu/over/GraphOverhead.csv', shell=True)
    File4=open('over/GraphOverhead.csv','a')
    File4.write(str(distancia)+','+str(overhead)+'\n')
    File4.close()

while True:
    
    # Mostramos el menu
    ff=createFileoverhead()
    menu()
    # solicituamos una opción al usuario
    opcionMenu = input("Escoja la opcion que desea ejecutar:")

    if opcionMenu == "1":
        exportdata()

    elif opcionMenu == "2":
        distancia=input('Escriba la distancia de transmisión en metros: ')
        print(len(str(distancia)))
        createFile()
        createFileoverhead()
        transmitdata(ff,distancia)
        sizefile=subprocess.getoutput('du -b ~/labcomu/over/OverheadDATA'+str(distancia)+ff+'.csv')
        sizefile1=subprocess.getoutput('du -b ~/labcomu/DatosGPS.csv')
        overhead=(int(sizefile[0:3])/int(sizefile1[0:3]))*100
        goverhead(distancia,overhead)
        print('Tamaño del archivo enviado= ' +str(sizefile[0:(len(sizefile)-(len(str(distancia))+43))]) + 'bytes' )
        print('Tamaño del archivo original= ' +str(sizefile1[0:(len(sizefile1)-62)]) + 'bytes' )
        print('El porcentaje de OverHead es de= ' + str(overhead) + '%' )
    elif opcionMenu == "3":
        gitlab()
    
    elif opcionMenu == "4":
        graficaroverhead()
        
    elif opcionMenu == "5":
        break
    else:
        print ("")
        input("No has pulsado ninguna opción correcta...\nPulsa una tecla para continuar")